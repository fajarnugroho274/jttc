<?php
include_once("config.php");
$result = mysqli_query($mysqli, "SELECT * FROM kontrak ORDER BY id_kontrak DESC");
// print_r($result);
// die;
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>JTTC</title>
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Pegawai</span></a>
              <a class="nav-item nav-link" href="jabatan.php">Jabatan</a>
              <a class="nav-item nav-link active" href="kontrak.php">Kontrak</a>
          </div>
      </div>
  </nav>
  <div class="card">
    <div class="card-header text-center"><h4>Data Kontrak</h4></div>
    <div class="card-body">
        <a href="tambah_kontrak.php" class='btn 
        btn-sm btn-success pull-right'>
        <i class="fa fa-arrow-left"></i> Tambah Data Kontrak</a>
        <br/><br/>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>Durasi</td>
                    <td>Aksi</td>
                </tr>
                <?php  
                while($kontrak_data = mysqli_fetch_array($result)) {?>
                    <tr>
                        <td><?= $kontrak_data['durasi'] ?></td>
                        <td>
                            <a href="edit_kontrak.php?id=<?= $kontrak_data['id_kontrak'] ?>"class="btn btn-sm btn-warning"><i class="fa fa-wrench"></i>Edit</a>
                            <a href="delete.php?primary=<?= $kontrak_data['id_kontrak']?>&jenis=kontrak&id=id_kontrak"class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>Hapus</a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>