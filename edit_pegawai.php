<?php
include_once("config.php");
$jabatan = mysqli_query($mysqli, "SELECT * FROM jabatan");
$kontrak = mysqli_query($mysqli, "SELECT * FROM kontrak");
?>
<?php

include_once("config.php");

if(isset($_POST['update']))
{    
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $id_jabatan = $_POST['id_jabatan'];
    $id_kontrak = $_POST['id_kontrak'];
    // echo $id;
    // echo $nama;
    // echo $alamat;
    // echo $id_jabatan;
    // echo $id_kontrak;
    // die;
    
    $durasi=$_POST['durasi'];
    $result = mysqli_query($mysqli, "UPDATE pegawai SET id_jabatan='$id_jabatan', id_kontrak='$id_kontrak', nama='$nama',alamat='$alamat' WHERE id_pegawai=$id");
    header("Location: index.php");
}
?>


<?php
// Display selected user data based on id
// Getting id from url
$id = $_GET['id'];

// Fetech user data based on id
$result = mysqli_query($mysqli, "SELECT * FROM pegawai join kontrak ON kontrak.id_kontrak=pegawai.id_kontrak JOIN jabatan ON jabatan.id_jabatan=pegawai.id_jabatan WHERE id_pegawai=$id");

while($pegawai_data = mysqli_fetch_array($result))
{
   $nama = $pegawai_data['nama'];
   $alamat = $pegawai_data['alamat'];
   $id_kontrak = $pegawai_data['id_kontrak'];
   $id_jabatan = $pegawai_data['id_jabatan'];
}
echo $nama;
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>JTTC</title>
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="index.php">Pegawai <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="jabatan.php">Jabatan</a>
                <a class="nav-item nav-link" href="kontrak.php">Kontrak</a>
            </div>
        </div>
    </nav>
    <div class="card">
        <div class="card-header text-center"><h4>Data Pegawai</h4></div>
        <div class="card-body">
            <a href="index.php" class='btn 
            btn-sm btn-success pull-right'>
            <i class="fa fa-arrow-left"></i> Kembali</a>
            <br/><br/>
            <form name="" method="post" action="edit_pegawai.php">
                <table class="table stripped">
                    <tr> 
                        <td>Nama </td>
                        <td>:</td>
                        <td><input type="text" class="form-control"  name="nama" value=<?php echo $nama?>></td>
                    </tr>

                    <tr> 
                        <td>Alamat </td>
                        <td>:</td>
                        <td><input type="text" name="alamat" value=<?php echo $alamat;?>></td>
                    </tr>

                    <tr> 
                        <td>Kontrak </td>
                        <td>:</td>
                        <td>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">pilih</label>
                            </div>
                            <select class="custom-select" name="id_kontrak">
                                <option selected>Choose...</option>
                                <?php  
                                while($kon = mysqli_fetch_array($kontrak)) {?>
                                    <option value="<?= $kon['id_kontrak'] ?>"><?= $kon['durasi'] ?></option>
                                <?php }?>
                            </select>
                        </div>
                    </td>
                </tr>

                <tr> 
                    <td>Jabatan </td>
                    <td>:</td>
                    <td>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">pilih</label>
                        </div>
                        <select class="custom-select" name="id_jabatan">
                            <option selected>Choose...</option>
                            <?php  
                            while($jab = mysqli_fetch_array($jabatan)) {?>
                                <option value="<?= $jab['id_jabatan'] ?>"><?= $jab['nama_jabatan'] ?></option>
                            <?php }?>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <input type="hidden" name="id" value=<?php echo $_GET['id'];?>>
                <td><input type="submit" name="update" value="Update"></td>
            </tr>
        </table>
    </form>
</div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>