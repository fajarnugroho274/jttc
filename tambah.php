<?php
include_once("config.php");
$jabatan = mysqli_query($mysqli, "SELECT * FROM jabatan");
$kontrak = mysqli_query($mysqli, "SELECT * FROM kontrak");
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>
    <div class="container-fluid">
        <div class="card">
            <div class="card-header text-center"><h4>Data Pegawai</h4></div>
            <div class="card-body">
                <form action="tambah.php" method="post" name="form1">
                    <table width="25%" border="0">
                        <tr> 
                            <td>Nama</td>
                            <td><input type="text" name="nama"></td>
                        </tr>
                
                        <tr> 
                            <td>Jabatan</td>
                            <td>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">pilih</label>
                                </div>
                                <select class="custom-select" name="id_jabatan">
                                    <option selected>Choose...</option>
                                <?php  
                                while($jab = mysqli_fetch_array($jabatan)) {?>
                                    <option value="<?= $jab['id_jabatan'] ?>"><?= $jab['nama_jabatan'] ?></option>
                                <?php }?>
                                </select>
                                </div>
                            </td>
                        </tr>

                         <tr> 
                            <td>Kontrak</td>
                            <td>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">pilih</label>
                                </div>
                                <select class="custom-select" name="id_kontrak">
                                    <option selected>Choose...</option>
                                    <?php  
                                while($kon = mysqli_fetch_array($kontrak)) {?>
                                    <option value="<?= $kon['id_kontrak'] ?>"><?= $kon['durasi'] ?></option>
                                <?php }?>
                                </select>
                                </div>
                            </td>
                        </tr>

                        <tr> 
                            <td>Alamat</td>
                            <td><input type="text" name="alamat"></td>
                        </tr>
                    <tr> 
                        <td></td>
                        <td><input type="submit" name="Submit" value="Tambah"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>

<?php
 
    // Check If form submitted, insert form data into users table.
    if(isset($_POST['Submit'])) {
        $nama = $_POST['nama'];
        $id_jabatan = $_POST['id_jabatan'];
        $id_kontrak = $_POST['id_kontrak'];
        $alamat = $_POST['alamat'];

        // print_r($id_kontrak);
        // die;
        
        // include database connection file
        include_once("config.php");
                
        // Insert user data into table
        $result = mysqli_query($mysqli, "INSERT INTO pegawai(id_pegawai, id_jabatan, id_kontrak, nama, alamat) VALUES ('', '$id_jabatan', '$id_kontrak', '$nama', '$alamat')");

        // INSERT INTO `pegawai` (`id_pegawai`, `id_jabatan`, `id_kontrak`, `nama`, `alamat`) VALUES ('2', '1', '1', 'w', 'w');

        // $result = mysqli_query($mysqli, "INSERT INTO users(name,email,mobile) VALUES('$name','$email','$mobile')");
        
        // Show message when user added
        echo "User added successfully. <a href='index.php'>View Users</a>";
    }
    ?>