<?php

include_once("config.php");

if(isset($_POST['update']))
{    
    $id = $_POST['id'];
    
    $durasi=$_POST['durasi'];
    $result = mysqli_query($mysqli, "UPDATE kontrak SET durasi='$durasi' WHERE id_kontrak=$id");
    header("Location: kontrak.php");
}
?>


<?php
// Display selected user data based on id
// Getting id from url
$id = $_GET['id'];

// Fetech user data based on id
$result = mysqli_query($mysqli, "SELECT * FROM kontrak WHERE id_kontrak=$id");

while($kontrak_data = mysqli_fetch_array($result))
{
   $durasi = $kontrak_data['durasi'];
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>JTTC</title>
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Pegawai <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link " href="jabatan.php">Jabatan</a>
                <a class="nav-item nav-link active" href="kontrak.php">Kontrak</a>
            </div>
        </div>
    </nav>
    <div class="card">
        <div class="card-header text-center"><h4>Data Jabatan</h4></div>
        <div class="card-body">
            <a href="kontrak.php" class='btn 
            btn-sm btn-success pull-right'>
            <i class="fa fa-arrow-left"></i> Kembali</a>
            <br/><br/>
            <form name="edit_jabatan" method="post" action="edit_kontrak.php">
                <table border="0">
                    <tr> 
                        <td>Durasi </td>
                        <td>:</td>
                        <td><input type="text" name="durasi" value=<?php echo $durasi;?>></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <input type="hidden" name="id" value=<?php echo $_GET['id'];?>>
                        <td><input type="submit" name="update" value="Update"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>